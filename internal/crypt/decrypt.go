package crypt

import (
	"crypto/aes"
	"crypto/cipher"
)

func Decrypt(encryptedMessage, key string) (*string, error) {

	keyBytes := []byte(key)
	encryptedMessageBytes := []byte(encryptedMessage)

	cipherBlock, err := aes.NewCipher(keyBytes)

	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(cipherBlock)

	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()

	if len(encryptedMessageBytes) < nonceSize {
		return nil, err
	}

	nonce, encryptedMessageBytes := encryptedMessageBytes[:nonceSize], encryptedMessageBytes[nonceSize:]

	message, err := gcm.Open(nil, nonce, encryptedMessageBytes, nil)

	if err != nil {
		return nil, err
	}

	stringMessage := string(message)

	return &stringMessage, nil
}
