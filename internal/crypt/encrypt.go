package crypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
)

func Encrypt(message, key string) (*string, error) {

	messageBytes := []byte(message)
	keyBytes := []byte(key)

	cipherBlock, err := aes.NewCipher(keyBytes)

	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(cipherBlock)

	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize()) // Standard 12 bytes

	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	encryptedMessageBytes := gcm.Seal(nonce, nonce, messageBytes, nil)

	encryptedMessageStr := string(encryptedMessageBytes)

	return &encryptedMessageStr, nil
}
