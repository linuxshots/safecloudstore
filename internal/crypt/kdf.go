package crypt

import (
	"gitlab.com/linuxshots/safecloudstore/config"
	"golang.org/x/crypto/scrypt"
)

func GenerateKey(password, salt string, aesconfig config.KDFConfiguration) (string, error) {
	passwordBytes := []byte(password)
	saltBytes := []byte(salt)

	derivedKey, err := scrypt.Key(passwordBytes, saltBytes, aesconfig.CostFactor, aesconfig.BlockSizeFactor, aesconfig.ParallelizationFactor, aesconfig.KeySize)

	if err != nil {
		return "", err
	}

	return string(derivedKey), nil
}
