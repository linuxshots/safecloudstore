package main

import (
	"fmt"

	"gitlab.com/linuxshots/safecloudstore/config"
	"gitlab.com/linuxshots/safecloudstore/internal/crypt"
	"gitlab.com/linuxshots/safecloudstore/service"
)

func main() {

	message, err := service.ReadMessage("Enter a message")

	if err != nil {
		fmt.Println("Error: Failed to read your message.")
		fmt.Println(err.Error())
		return
	}

	password, err := service.ReadPassword("Enter encryption password")
	if err != nil {
		fmt.Println("Error: Failed to read your password.")
		fmt.Println(err.Error())
		return
	}

	salt, err := service.GenerateRandomString(8)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	aesKey, err := crypt.GenerateKey(password, salt, config.AESConfig)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Encrypting...")
	encryptedEncodedMessage, err := crypt.Encrypt(message, aesKey)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Encrypted Message: ", *encryptedEncodedMessage)

	fmt.Println("Decrypting...")

	password, err = service.ReadPassword("Enter decryption password")

	if err != nil {
		fmt.Println("Error: Failed to read your password.")
		fmt.Println(err.Error())
		return
	}

	aesKey, err = crypt.GenerateKey(password, salt, config.AESConfig)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	decryptedMessage, err := crypt.Decrypt(*encryptedEncodedMessage, aesKey)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(*decryptedMessage)
}
