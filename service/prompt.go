package service

import (
	"bufio"
	"fmt"
	"os"
)

func ReadPassword(prompt string) (string, error) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print(prompt + " > ")

	password, err := reader.ReadString('\n')
	if err != nil {
		return "", err
	}

	return password, nil
}

func ReadMessage(prompt string) (string, error) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print(prompt + " > ")
	message, err := reader.ReadString('\n')

	if err != nil {
		fmt.Println("Error: Failed to read your message.")
		fmt.Println(err.Error())
		return "", err
	}

	return message, nil
}
