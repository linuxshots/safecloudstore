package service

import (
	"math/rand"
	"time"
)

func GenerateRandomString(size int) (string, error) {
	token := make([]byte, size)

	rand.Seed(time.Now().UnixNano())
	_, err := rand.Read(token)

	if err != nil {
		return "", err
	}

	return string(token), nil
}
